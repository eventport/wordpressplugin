<?php
/*
	Plugin Name: Eventport
	Plugin URI: http://www.eventport.net
	Description: Official plugin for Eventport API
	Author: Dariusz Oleksy
	Version: 0.1-alpha
	Author URI: http://www.dariuszoleksy.de
	Text Domain: eventport
 */

//CONFIG
require_once('php/config.php');

header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past

//PLUGIN
function Eventport_activation() {
    if ( ! current_user_can( 'activate_plugins' ) )
        return;
    global $accessToken;
    $url = 'http://app.eventport.net/checkToken/'.$accessToken;
    $options = array(
        'http' => array(
            'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
            'method'  => 'GET',
        ),
    );
    $context  = stream_context_create($options);
    $result = file_get_contents($url, false, $context);
    if($http_response_header[0] != 'HTTP/1.1 200 OK'){
        $html = '<div class="error">';
        $html .= '<p>';
        $html .= 'Eventport-plugin konnte nicht authentifiziert werden. überprüfen Sie bitte den Zugriffsschlüssel.';
        $html .= '</p>';
        $html .= '</div><!-- /.updated -->';
        echo $html;

        return false;
    }

    require_once( ABSPATH . '/wp-admin/includes/upgrade.php' );
    global $wpdb;
    $db_table_name = $wpdb->prefix . 'Eventport_events';
    if( $wpdb->get_var( "SHOW TABLES LIKE '$db_table_name'" ) != $db_table_name ) {
        if ( ! empty( $wpdb->charset ) )
            $charset_collate = "DEFAULT CHARACTER SET $wpdb->charset";
        if ( ! empty( $wpdb->collate ) )
            $charset_collate .= " COLLATE $wpdb->collate";

        $sql = "CREATE TABLE " . $db_table_name . " (
			`profile_id` varchar(100) NOT NULL,
			`event_id` varchar(100) NOT NULL,
			`data` longtext NOT NULL,
			`post_id` varchar(100),
			`meta` longtext,
			`date_time` TIMESTAMP,
			PRIMARY KEY (`event_id`),
			UNIQUE (`post_id`)
		) $charset_collate;";
        dbDelta( $sql );
    }
    add_option( 'Activated_Plugin', 'Eventport' );
}

function Eventport_deactivation(){
    if ( ! current_user_can( 'activate_plugins' ) )
        return;
    global $wpdb;
    $db_table_name = $wpdb->prefix . 'Eventport_events';
    $wpdb->query('DROP TABLE IF EXISTS '.$db_table_name);
}
register_activation_hook(__FILE__, 'Eventport_activation');

register_deactivation_hook(__FILE__, 'Eventport_deactivation');
?>