<?php

require_once('statusCodes.php');

require_once('../../../../wp-config.php');
require_once('../../../../wp-blog-header.php');
require_once('../../../../wp-load.php');
require_once('../../../../wp-includes/pluggable.php');
require_once('config.php');



//check token
$token = $_GET['accessToken'];
if (!in_array($token, $accessToken)) {
    http_response_code(403);
    exit;
} else {
    //login as admin
    wp_set_current_user( 1 );
    // Old Eventport API
    $json = $_POST['data'];
    if (empty($json)) {
        // New Eventport API
        $json = file_get_contents('php://input');
        $data = json_decode($json, true);
    } else{
        // Old Eventport API
        $data = json_decode(stripslashes($json), true);
    }
    if ($json != null && isset($data['_id'])) {
        //check if data should be inserted/updated or deleted
        if (!isset($data['delete'])) {
            $existingEntry = $wpdb->get_row('SELECT * FROM ' . $wpdb->prefix . 'Eventport_events WHERE event_id = "' . addslashes($data['_id']) . '"');
            $wpdb->replace($wpdb->prefix . 'Eventport_events', array('event_id' => addslashes($data['_id']), 'profile_id' => addslashes($data['profileId']), 'data' => $json));
            if (isset($existingEntry->post_id)) {
                custom_update_handler($data, $json, $existingEntry->post_id);
            } else {
                custom_insert_handler($data, $json);
            }
        } else {
            //get entry from Eventport-table
            $existingEntry = $wpdb->get_row('SELECT * FROM ' . $wpdb->prefix . 'Eventport_events WHERE event_id = "' . addslashes($data['_id']) . '"');
            //delete WP-Post
            $toDelete = $wpdb->delete($wpdb->prefix . 'Eventport_events', array('event_id' => $data['_id']));
            if ($toDelete != null) {
                custom_delete_handler($data['_id'], $existingEntry->post_id);
            }
        }
    }
}

function custom_insert_handler($data, $postData)
{
    //prepare event description
    $desc = $data['description'];
    if ($data['channels']['homepage']['useGeneralDescription'] == true) {
        $desc = $data['channels']['homepage']['description'];
    }
    //prepare categories
    $categories = array();
    if (isset($data['types'])) {
        for ($i = 0; $i < sizeOf($data['types']); $i++) {
            $cat = get_cat_ID($data['types'][$i]);
            if ($cat != null) {
                array_push($categories, $cat);
            }
        }
    }
    //prepare WP-post
    $new_post = array(
        'post_title' => $data['name'],
        'post_content' => $desc,
        'post_status' => 'future',
        'post_date' => date('Y-m-d H:i:s', strtotime($data[begin])),
        'post_author' => 1,
        'post_type' => 'post',
        'post_category' => $categories
    );
    //insert post
    $post_id = wp_insert_post($new_post, true);
    //insert additional post-data
    updatePostData($post_id, $data);

}

function custom_update_handler($data, $postData, $post_id)
{
    //prepare description
    $desc = $data['channels']['homepage']['description'];
    if ($data['channels']['homepage']['useGeneralDescription'] == true) {
        $desc = $data['description'];
    }
    //prepare categories
    $categories = array();
    if (isset($data['types'])) {
        for ($i = 0; $i < sizeOf($data['types']); $i++) {
            $cat = get_cat_ID($data['types'][$i]);
            if ($cat != null) {
                array_push($categories, $cat);
            }
        }
    }
    //prepare WP-post
    $new_post = array(
        'ID' => $post_id,
        'post_title' => $data['name'],
        'post_content' => $desc,
        'post_status' => 'future',
        'post_date' => date('Y-m-d H:i:s', strtotime($data[begin])),
        'post_author' => 1,
        'post_type' => 'post',
        'post_category' => $categories
    );
    //update post
    $post_id = wp_update_post($new_post, true);
    //update additional data
    updatePostData($post_id, $data);

}

function custom_delete_handler($event_id, $post_id)
{
    //delete post
    wp_delete_post($post_id, true);
    http_response_code(200);
    echo 'event deleted';
}

function updatePostData($post_id, $data)
{
    if(!is_int($post_id)){
        http_response_code(400);
        exit;
    }
    global $wpdb, $url_prefix, $url_postfix;
    //delete thumbnail
    delete_post_thumbnail( $post_id );
    //update eventport-table with new post-id
    $wpdb->update($wpdb->prefix . 'Eventport_events', array('post_id' => $post_id), array('event_id' => $data['_id']));
    //download thumbnail
    if (isset($data['homepagePhoto'])) {
        // Check folder permission and define file location
        $upload_dir = wp_upload_dir(); // Set upload folder
        $image_url = $data['homepagePhoto']; // Define the image URL here
        $filename = rawurldecode(basename($image_url)); // Create image file name
        if (wp_mkdir_p($upload_dir['path'])) {
            $file = $upload_dir['path'] . '/' . $filename;
        } else {
            $file = $upload_dir['basedir'] . '/' . $filename;
        }
        //download file
        $stream = fopen($image_url, 'r');
        if($stream){
            file_put_contents($file, $stream);
            fclose($stream);
        }

        // Check image file type
        $wp_filetype = wp_check_filetype($filename, null);

        // Set attachment data
        $attachment = array(
            'post_mime_type' => $wp_filetype['type'],
            'post_title' => sanitize_file_name($filename),
            'post_content' => '',
            'post_status' => 'inherit'
        );

        $attach_id = wp_insert_attachment($attachment, $file, $post_id);
        require_once(ABSPATH . 'wp-admin/includes/image.php');
        $attach_data = wp_generate_attachment_metadata($attach_id, $file);
        wp_update_attachment_metadata($attach_id, $attach_data);
        set_post_thumbnail($post_id, $attach_id);
    }
    //add media-links
    if (is_array($data['media'])) {
        for ($i = 0; $i < sizeof($data['media']); $i++) {

            if (strpos($data['media'][$i], 'soundcloud.com') !== false) {
                add_post_meta($post_id, 'post_soundcloud', $data['media'][$i], true) || update_post_meta($post_id, 'soundcloud', $data['media'][$i]);
            }
            if (strpos($data['media'][$i], 'facebook.com') !== false) {
                add_post_meta($post_id, 'post_face', $data['media'][$i], true) || update_post_meta($post_id, 'facebook', $data['media'][$i]);
            }

        }
    }
    if(is_int($post_id)){
        http_response_code(200);
        echo $url_prefix. $post_id . $url_postfix;
    } else{
        http_response_header(400);
    }
}

?>